resource "aws_s3_bucket" "bucket" {
  bucket = "calsoft-demo-bucket"
  acl    = "private"

  tags = {
    Name        = "calsoft-demo-bucket"
  }
}
