#!/bin/bash
RED="\033[31m"
YELLOW="\033[33m"
GREEN="\033[32m"
BLUE="\033[34m"
NC="\033[m"
init_dir=".terraform"
if [ -d $init_dir ];
then
    echo -e "${RED}Already initialization done${NC}"
else
    echo -e "${BLUE}Start initialization${NC}"
    terraform init
    echo -e "${YELLOW}Initialization done${NC}"
fi
